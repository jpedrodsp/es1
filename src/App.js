import { Fabric } from 'office-ui-fabric-react/lib/Fabric';
import React, { Component } from 'react';
import './App.css';
import HandleContent from './components/handleContent';
import Nav from './components/nav-bar';


class App extends Component {
  state = {
    content: 'Home',
    islogged: false
  }
  content = (name) => {
    if (name === 'Home') {
      this.setState({
        content: name
      })
    }
    if (name === 'Reserva de Recurso') {
      this.setState({
        content: name
      })
    }
    if (name === 'Alocação de recurso') {
      this.setState({
        content: name
      })
    }
  }
  render() {
    return (
      <Fabric>
        <div className="App" >
          <header className="App-header">
            <p>Bem vindo ao ProGR</p>
          </header>
          <div className="body">
            <div className="App-nav">
              <Nav content={this.content} islogged={this.state.islogged} />
            </div>
            <div></div>
            <HandleContent content={this.state.content} />
            <button onClick={() => { this.state.islogged ? this.setState({ islogged: false }) : this.setState({ islogged: true }) }}>clisk me</button>
          </div>
        </div>
      </Fabric>
    );
  }
}

export default App;