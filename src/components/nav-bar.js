import React from 'react';
import { Nav } from 'office-ui-fabric-react/lib/Nav';
import { initializeIcons } from '@uifabric/icons';
initializeIcons();

const NavFabric  = (props) => {
  const _logTrue = (props.islogged ? false : true)
  const _onLinkClick = (ev, item) => {
    if (item && item.name === 'Home') {
      props.content(item.name)
    }
    if (item && item.name === 'Reserva de Recurso') {
      props.content(item.name)
    }
    if (item && item.name === 'Alocação de recurso') {
      props.content(item.name)
    }
    

  }
  return (
    <Nav
      onLinkClick={_onLinkClick}
      expandedStateText="expanded"
      collapsedStateText="collapsed"
      selectedKey="key3"
      expandButtonAriaLabel="Expand or collapse"
      styles={{
        root: {
          width: 300,
          boxSizing: 'border-box',
          overflowY: 'auto'
        }
      }}
      groups={[
        {
          links: [
            {
              name: 'Home',
              url: '#',
              isExpanded: true
            },
            {
              name: 'Recurso',
              isExpanded: true,
              links: [
                {
                  name: 'Reserva de Recurso',
                  url: '#/recurso/reserva',
                  key: 'recursoreserva',
                  disabled: _logTrue
                },
                {
                  name: 'Alocação de recurso',
                  url: '#/recurso/alocacao',
                  key: 'recursoalocacao',
                  disabled: _logTrue
                }
              ]
            }
          ]
        }
      ]}
    />
  );
};

export default NavFabric;