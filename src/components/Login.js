import React, {Component} from 'react'
import {Label, TextField, PrimaryButton} from 'office-ui-fabric-react'
import Error from './Error'

class Login extends Component {
    state = {
        error: 0,
        user: undefined,
        pass: undefined
    }
    handleUserChange = (e) => {
        const newstate = this.state
        newstate.user = e.target.value
        this.setState(newstate)
    }
    handlePasswordChange = (e) => {
        const newstate = this.state
        newstate.pass = e.target.value
        this.setState(newstate)
    }
    handleSubmit = (e) => {
        e.preventDefault()
        console.log('login -> "' + this.state.user + '":"' + this.state.pass + '"')
    }
    render() {
        return(
            <div className="login-container">
                <Error msg={'Insira detalhes de login válidos.'} />
                <form onSubmit={this.handleSubmit}>
                    <TextField label="Usuário" onChange={this.handleUserChange} onSubmit={this.handleSubmit}/>
                    <TextField label="Senha" maskChar={'*'} type="password" onChange={this.handlePasswordChange} onSubmit={this.handleSubmit}/>
                    <PrimaryButton onClick={this.handleSubmit}>Login</PrimaryButton>
                </form>
            </div>
        )
    }
}

export default Login