import React from 'react';
import '../App.css';
import writeNewSector from './funcoes_bd/writeNewSector'
import writeNewFunc from './funcoes_bd/writeNewFunc'
import readOneSector from './funcoes_bd/readOneSector'
import Solicitacao from './solicitacao/solicitacao';
import CRUD_Recurso from './crud/recurso/crud_recurso';
import CRUD_Usuario from './crud/usuario/crud_usuario';
// import firebase from 'firebase'

var firebase = require("firebase");
    const firebaseConfig = {
        apiKey: "AIzaSyDmIw5KpPrGMcqLTWLJRvp4NYlLwTHKFgE",
        authDomain: "bd-progr.firebaseapp.com",
        databaseURL: "https://bd-progr.firebaseio.com",
        projectId: "bd-progr",
        storageBucket: "bd-progr.appspot.com",
        messagingSenderId: "83149954819",
        appId: "1:83149954819:web:88bdab18bdb8cadb"
    };
    firebase.initializeApp(firebaseConfig);

class Home extends React.Component{
    state = {
        text: '',
        inBD: ''
    }
    handleChanges = (e) =>{
        this.setState({
            text: e.target.value
        })
    }
    handleClick=()=>{
        // writeNewSector('Casas Bahia', firebase)
        var a = readOneSector(firebase, 'Casas Bahia')
        var func = {
            login: 'João',
            senha: '01234',
            cpf: '99999999999',
            nome: 'João Pedro',
            telefone: 'Não tem',
            endereco: 'Algum por ai'
        }
        console.log(a)
        writeNewFunc(firebase, func, '-Lh_roaBUfVbz7x87D20')
    }
    render(){
        return (
            <div className="Home">
                <p>Estou na pagina Home</p>
                <input type='text' onChange={this.handleChanges}></input>
                <button onClick={this.handleClick}>Clique me</button>
                <button onClick={this.componentDidMount}>Carregar from BD</button>
                <CRUD_Recurso/>
                <CRUD_Usuario/>
            </div>
        );
    }
}

export default Home;