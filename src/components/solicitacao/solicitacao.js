import React, {Component} from 'react'
import PesquisaSolicitacao from './pesquisa_solicitacao';
import CriarSolicitacao from './criar_solicitacao'
import ResultadosPesquisa from './resultados_pesquisa';

class Solicitacao extends Component {
    render() {
        return(
            <div className="solicitacao-container">
                <PesquisaSolicitacao />
                <ResultadosPesquisa />
                <CriarSolicitacao />
            </div>
        )
    }
}

export default Solicitacao