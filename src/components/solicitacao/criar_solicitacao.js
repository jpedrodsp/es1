import React, {Component} from 'react'
import { Label, PrimaryButton, TextField, Dropdown, Calendar, DatePicker, SpinButton} from 'office-ui-fabric-react'

class CriarSolicitacao extends Component {
    state = {
        recurso: undefined,
        solicitante: undefined
    }
    render() {
        return (
            <div className="solicitar-recurso-container">
                <Label>Informações de Recurso</Label>
                <TextField label={'Nome do Recurso'} disabled={true}/>
                <TextField label={'Solicitante'} disabled={true}/>
                <TextField label={'Responsável por Recurso'} disabled={true}/>
                <TextField label={'Tempo de permanência máximo com recurso'} disabled={true}/>
                <TextField label={'Tempo de antecedência mínimo para solicitação de recurso'} disabled={true}/>
                <Label>Data de Início:</Label>
                <DatePicker label={'Dia'}></DatePicker>
                <SpinButton label={'Horário de Início'}></SpinButton>
                <Label>Duração de solicitação:</Label>
                <SpinButton label={'Hora(s)'}></SpinButton>
                <PrimaryButton>Solicitar recurso</PrimaryButton>
            </div>
        )
    }
}

export default CriarSolicitacao