import React, {Component} from 'react'
import { Label, PrimaryButton, TextField, Dropdown } from 'office-ui-fabric-react'

class PesquisaSolicitacao extends Component {
    state = {
        pesquisa: '',
        departamento: '',
        resultados: []
    }
    render() {
        return (
            <div className="pesquisa-solicitacao-container">
                <Label>Pesquisa de Recurso</Label>
                <TextField label={'Nome do Recurso'}/>
                <Dropdown label={'Departamento pertencente'}></Dropdown>
                <PrimaryButton>Pesquisar</PrimaryButton>
            </div>
        )
    }
}

export default PesquisaSolicitacao