import React, {Component} from 'react'
import { ChoiceGroup } from 'office-ui-fabric-react'

class ResultadosPesquisa extends Component {
    state = {
        resultados: [
            {list_uid: 0, resource_id: 0, departament_id: 0, name: 'Recurso teste 1', portable: false, horas_antecedencia: 7, horas_maxima: 48},
            {list_uid: 1, resource_id: 1, departament_id: 0, name: 'Recurso teste 2', portable: true, horas_antecedencia: 16, horas_maxima: 64},
            {list_uid: 2, resource_id: 2, departament_id: 0, name: 'Recurso teste 3', portable: false, horas_antecedencia: 8, horas_maxima: 48},
            {list_uid: 3, resource_id: 3, departament_id: 0, name: 'Recurso teste 4', portable: true, horas_antecedencia: 10, horas_maxima: 64},
            {list_uid: 4, resource_id: 4, departament_id: 0, name: 'Recurso teste 5', portable: false, horas_antecedencia: 2, horas_maxima: 48}
        ]
    }
    render(){
        const lista_resultados = this.state.resultados.map(item => {
            let display_name = item.name;
            if (item.portable) display_name += ' (Portátil)';
            return(
                {key: item.list_uid,
                text: display_name}
            )
        })
        return(
            <div className="resultados-pesquisa-container">
                <ChoiceGroup
                className="defaultChoiceGroup"
                options={lista_resultados}
                // onChange={this._onChange}
                label="Resultados da Pesquisa"
                required={true}
                />
            </div>
        )
    }
}

export default ResultadosPesquisa