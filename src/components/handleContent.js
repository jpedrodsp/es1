import React from 'react';
import Reserva from './reserva';
import Alocacao from './alocação';
import Home from './home';


const HandleContent = ({content}) =>{
    const nc = () => {
            switch (content) {
            case 'Home':
                return (<Home/>)
            case 'Reserva de Recurso':
                return (<Reserva/>)
            case 'Alocação de recurso':
                return (<Alocacao/>)
            default:
                return (<p>Error!</p>)
        }
    }
    const onContent = content === 'Home' ? (
        <Home/>
    ) : (
        content === 'Reserva de Recurso' ? (
            <Reserva/>
        ) : (
            content === 'Alocação de recurso' ?(
                <Alocacao/>
            ) : null
        )
    )
    return(
        <div>
            {onContent}
        </div>
    )
}

export default HandleContent;