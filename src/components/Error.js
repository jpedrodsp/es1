import React, { Component } from "react";
import { MessageBarButton } from 'office-ui-fabric-react/lib/Button';
import { Link } from 'office-ui-fabric-react/lib/Link';
import { Label } from 'office-ui-fabric-react/lib/Label';
import { Stack, StackItem } from 'office-ui-fabric-react/lib/Stack';
import { MessageBar, MessageBarType } from 'office-ui-fabric-react/lib/MessageBar';

class ErrorBalloon extends Component {
    state = {
        msg: this.props.msg
    }
    render(){
        return(
            <div className="error-balloon">
                <MessageBar messageBarType={MessageBarType.error} isMultiline={false} onDismiss={() => console.log('test')} dismissButtonAriaLabel="Close">
                    {this.state.msg}
                </MessageBar>
            </div>
        )
    }
}

export default ErrorBalloon