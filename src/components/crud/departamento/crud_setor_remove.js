import React, {Component} from 'react'
import {TextField, Label, PrimaryButton} from 'office-ui-fabric-react'

class CRUD_Setor_Remove extends Component {
    state = {
        id: 0
    }
    handleChange = (e) => {
        const newstate = {
            id: e.target.value
        }
        this.setState(newstate)
        console.log(e.target.value)
    }
    render() {
        return(
            <div className="crud-recurso-remove-container">
                <form onSubmit={() => this.props.removeRecurso(this.state.id)}>
                    <Label>Remover Recurso</Label>
                    <TextField label={"Remover recurso (id):"} onSubmit={() => this.props.removeRecurso(this.state.id)} onChange={this.handleChange}></TextField>
                    <PrimaryButton onClick={() => this.props.removeRecurso(this.state.id)}>Remover recurso</PrimaryButton>
                </form>
            </div>
        )
    }
}

export default CRUD_Setor_Remove