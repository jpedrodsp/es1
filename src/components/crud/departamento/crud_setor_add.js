import React, {Component} from 'react'
import {TextField, Label, PrimaryButton, Checkbox} from 'office-ui-fabric-react'

class CRUD_Setor_Add extends Component {
    state = {
        id: Math.random(),
        nome: ''
    }
    handleChangeNome = (e) => {
        let newstate = this.state
        newstate.nome = e.target.value
        this.setState(newstate)
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.addSetor(this.state)
        let copy = this.state
        copy.id = Math.random()
        this.setState(copy)
    }
    render(){
        return(
            <div className="crud-setor-add-container">
                <form onSubmit={this.handleSubmit}>
                    <Label>Adicionar Setor</Label>
                    <TextField label={"Nome"} onChange={this.handleChangeNome}/>
                    <PrimaryButton onClick={() => this.props.addSetor(this.state)}>Adicionar Setor</PrimaryButton>
                </form>
            </div>
        )
    }
}

export default CRUD_Setor_Add