import React, {Component} from 'react'
import {TextField, Label, PrimaryButton} from 'office-ui-fabric-react'

class CRUD_Setor extends Component {
    state = {
        selected: undefined,
        content: [
            {id: 0, nome: 'Setor'},
        ]
    }
    addSetor = (sector) => {
        sector.id = Math.random()
        let newstate = this.state
        const newcontent = [...this.state.content, sector]
        newstate.content = newcontent
        this.setState(newstate)
    }
    removeSetor = (resid) => {
        const newcontent = this.state.content.filter(item => {
            return item.id.toString() !== resid
        })
        let newstate = this.state
        newstate.content = newcontent
        this.setState(newstate)
    }
    render(){
        const list = this.state.content.map(item => {
            return (
                <div className="sector-list-item" key={item.id}>
                    <p>{item.id} {item.nome} {item.descricao} {item.portatil.toString()}</p>
                </div>
            )
        })
        return(
            <div className="crud-usuario-container">
                <div className="selected-sector-detail">
                    <form>
                        <Label>Selected Sector</Label>
                        <TextField label={"ID"} disabled={true}></TextField>
                        <TextField label={"Nome"} disabled={true}></TextField>
                    </form>
                </div>
                <div className="sector-add">
                    <CRUD_Setor_Add addSetor={this.addSetor}/>
                </div>
                <div className="sector-list">
                    {list}
                </div>
                <div className="sector-del">
                    <CRUD_Setor_Remove removeSetor={this.removeSetor}/>
                </div>
            </div>
        )
    }
}

export default CRUD_Setor