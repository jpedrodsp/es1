import React, {Component} from 'react'
import {TextField, Label, PrimaryButton} from 'office-ui-fabric-react'
import CRUD_Recurso_Add from './crud_recurso_add';
import CRUD_Recurso_Remove from './crud_recurso_remove'

class CRUD_Recurso extends Component {
    state = {
        selected: undefined,
        content: [
            {id: 0, nome: 'Recurso', descricao: '', portatil: false},
        ]
    }
    addRecurso = (resource) => {
        resource.id = Math.random()
        let newstate = this.state
        const newcontent = [...this.state.content, resource]
        newstate.content = newcontent
        this.setState(newstate)
    }
    removeRecurso = (resid) => {
        const newcontent = this.state.content.filter(item => {
            return item.id.toString() !== resid
        })
        let newstate = this.state
        newstate.content = newcontent
        this.setState(newstate)
    }
    render(){
        const list = this.state.content.map(item => {
            return (
                <div className="resource-list-item" key={item.id}>
                    <p>{item.id} {item.nome} {item.descricao} {item.portatil.toString()}</p>
                </div>
            )
        })
        return(
            <div className="crud-usuario-container">
                <div className="selected-user-detail">
                    <form>
                        <Label>Selected Resource</Label>
                        <TextField label={"ID"} disabled={true}></TextField>
                        <TextField label={"Nome"} disabled={true}></TextField>
                        <TextField label={"Descrição"} disabled={true}></TextField>
                        <TextField label={"Portatil"} disabled={true}></TextField>
                    </form>
                </div>
                <div className="user-add">
                    <CRUD_Recurso_Add addRecurso={this.addRecurso}/>
                </div>
                <div className="user-list">
                    {list}
                </div>
                <div className="user-del">
                    <CRUD_Recurso_Remove removeRecurso={this.removeRecurso}/>
                </div>
            </div>
        )
    }
}

export default CRUD_Recurso