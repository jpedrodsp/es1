import React, {Component} from 'react'
import {TextField, Label, PrimaryButton, Checkbox} from 'office-ui-fabric-react'

class CRUD_Recurso_Add extends Component {
    state = {
        id: Math.random(),
        nome: '',
        descricao: '',
        portatil: false
    }
    handleChangeNome = (e) => {
        let newstate = this.state
        newstate.nome = e.target.value
        this.setState(newstate)
    }
    handleChangeDescricao = (e) => {
        let newstate = this.state
        newstate.descricao = e.target.value
        this.setState(newstate)
    }
    handleChangePortability = (e) => {
        let newstate = this.state
        newstate.portatil = e.target.checked
        this.setState(newstate)
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.addRecurso(this.state)
        let copy = this.state
        copy.id = Math.random()
        this.setState(copy)
    }
    render(){
        return(
            <div className="crud-recurso-add-container">
                <form onSubmit={this.handleSubmit}>
                    <Label>Adicionar Recurso</Label>
                    <TextField label={"Nome"} onChange={this.handleChangeNome}/>
                    <TextField label={"Descrição"} onChange={this.handleChangeDescricao}/>
                    <Checkbox onChange={this.handleChangePortability} label={"Recurso Portátil"}/>
                    <PrimaryButton onClick={() => this.props.addRecurso(this.state)}>Adicionar Recurso</PrimaryButton>
                </form>
            </div>
        )
    }
}

export default CRUD_Recurso_Add