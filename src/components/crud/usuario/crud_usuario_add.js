import React, {Component} from 'react'
import {TextField, Label, PrimaryButton} from 'office-ui-fabric-react'

class CRUD_Usuario_Add extends Component {
    state = {
        id: Math.random(),
        nome: '',
        cpf: '',
        telefone: '',
        endereco: ''
    }
    handleChangeNome = (e) => {
        let newstate = this.state
        newstate.nome = e.target.value
        this.setState(newstate)
    }
    handleChangeCPF = (e) => {
        let newstate = this.state
        newstate.cpf = e.target.value
        this.setState(newstate)
    }
    handleChangeTelefone = (e) => {
        let newstate = this.state
        newstate.telefone = e.target.value
        this.setState(newstate)
    }
    handleChangeEndereco = (e) => {
        let newstate = this.state
        newstate.endereco = e.target.value
        this.setState(newstate)
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.addUser(this.state)
        let copy = this.state
        copy.id = Math.random()
        this.setState(copy)
    }
    render(){
        return(
            <div className="crud-user-add-container">
                <form onSubmit={this.handleSubmit}>
                    <Label>Adicionar Usuário</Label>
                    <TextField label={"Nome"} onChange={this.handleChangeNome} />
                    <TextField label={"CPF"} onChange={this.handleChangeCPF} />
                    <TextField label={"Telefone"} onChange={this.handleChangeTelefone} />
                    <TextField label={"Endereço"} onChange={this.handleChangeEndereco} />
                    <PrimaryButton onClick={() => this.props.addUser(this.state)}>Adicionar Usuário</PrimaryButton>
                </form>
            </div>
        )
    }
}

export default CRUD_Usuario_Add