import React, {Component} from 'react'
import {TextField, Label, PrimaryButton} from 'office-ui-fabric-react'

class CRUD_Usuario_Remove extends Component {
    state = {
        id: 0
    }
    handleChange = (e) => {
        const newstate = {
            id: e.target.value
        }
        this.setState(newstate)
        console.log(e.target.value)
    }
    render() {
        return(
            <div className="crud-usuario-remove-container">
                <form onSubmit={() => this.props.removeUser(this.state.id)}>
                    <Label>Remover Usuário</Label>
                    <TextField label={"Remover usuário (id):"} onSubmit={() => this.props.removeUser(this.state.id)} onChange={this.handleChange}></TextField>
                    <PrimaryButton onClick={() => this.props.removeUser(this.state.id)}>Remover usuário</PrimaryButton>
                </form>

            </div>
        )
    }
}

export default CRUD_Usuario_Remove