import React, {Component} from 'react'
import {TextField, Label, PrimaryButton} from 'office-ui-fabric-react'
import CRUD_Usuario_Add from './crud_usuario_add'
import CRUD_Usuario_Remove from './crud_usuario_remove'

class CRUD_Usuario extends Component {
    state = {
        selected: undefined,
        content: [
            {id: 0, nome: 'Nome de Usuário', cpf: '123.456.789-10', telefone: '12345678', endereco: 'Rua de Teste, Estado Inicial'},
            {id: 1, nome: 'Nome de Usuário', cpf: '123.456.789-10', telefone: '12345678', endereco: 'Rua de Teste, Estado Inicial'}
        ]
    }
    addUser = (user) => {
        user.id = Math.random()
        let newstate = this.state
        const newcontent = [...this.state.content, user]
        newstate.content = newcontent
        this.setState(newstate)
    }
    removeUser = (userid) => {
        const newcontent = this.state.content.filter(item => {
            return item.id.toString() !== userid
        })
        let newstate = this.state
        newstate.content = newcontent
        this.setState(newstate)
    }
    render(){
        const list = this.state.content.map(item => {
            return (
                <div className="user-list-item" key={item.id}>
                    <p>{item.id} {item.cpf} {item.telefone} {item.endereco}</p>
                </div>
            );
        })
        return(
            <div className="crud-usuario-container">
                <div className="selected-user-detail">
                    <form>
                        <Label>Selected User</Label>
                        <TextField label={"ID"} disabled={true}></TextField>
                        <TextField label={"Nome"} disabled={true}></TextField>
                        <TextField label={"CPF"} disabled={true}></TextField>
                        <TextField label={"Telefone"} disabled={true}></TextField>
                        <TextField label={"Endereço"} disabled={true}></TextField>
                    </form>
                </div>
                <div className="user-add">
                    <CRUD_Usuario_Add addUser={this.addUser}/>
                </div>
                <div className="user-list">
                    {list}
                </div>
                <div className="user-del">
                    <CRUD_Usuario_Remove removeUser={this.removeUser}/>
                </div>
            </div>
        )
    }
}

export default CRUD_Usuario