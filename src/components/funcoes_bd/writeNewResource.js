const writeNewResource = (firebase, state, setorKey, reponKey) => {
    var content = {
        nome: state.nome,
        descricao: state.desc
    }
    var newKey = firebase.database().ref('Setores/' + setorKey + '/Funcionarios/' + reponKey + '/Resources').push().key
    var rootRef = firebase.database().ref('Setores/' + setorKey + '/Funcionarios/' + reponKey + '/Resources').child(newKey)
    rootRef.set(content)
} 
export default writeNewResource;