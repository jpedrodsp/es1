const readAllResources = (firebase) => {
    var setorRef = firebase.database().ref('Setores');
    var Resources = []

    setorRef.on('value', function(setores) {
        setores.forEach(function(childSetor) {
            var funcRef = firebase.database().ref('Setores/' + childSetor.key + '/Funcionarios');
            funcRef.on('value', function(funcionarios){
                funcionarios.forEach(function(childFuncionarios){
                    var resourceRef = firebase.database().ref('Setores/' + childSetor.key + '/Funcionarios/' + childFuncionarios.key + '/Resources');
                    resourceRef.on('value', function(resources){
                        resources.forEach(function(childResource){
                            let funcionario = {
                                setorKey:  childSetor.key,
                                funcKey: childFuncionarios.key,
                                resouKey: childResource.key,
                                nome: childResource.nome,
                                descricao: childResource.desc
                            }
                            Resources = [...Resources, funcionario]
                        })
                    })
                })
            })
        });
    });
    return(
        {Resources}
    )
}

export default readAllResources;