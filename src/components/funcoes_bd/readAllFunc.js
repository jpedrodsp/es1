const readAllFunc = (firebase) => {
    var setorRef = firebase.database().ref('Setores');
    var Funcionarios = []

    setorRef.on('value', function(setores) {
        setores.forEach(function(childSetor) {
            var funcRef = firebase.database().ref('Setores/' + childSetor.key + '/Funcionarios');
            funcRef.on('value', function(funcionarios){
                funcionarios.forEach(function(childFuncionarios){
                    let funcionario = {
                        setorKey:  childSetor.key,
                        funcKey: childFuncionarios.key,
                        login: childFuncionarios.login,
                        senha: childFuncionarios.senha,
                        cpf: childFuncionarios.cpf,
                        nome: childFuncionarios.nome,
                        telefone: childFuncionarios.telefone,
                        endereco: childFuncionarios.endereco
                    }
                    Funcionarios = [...Funcionarios, funcionario]
                })
            })
        });
    });
    return(
        {Funcionarios}
    )
}

export default readAllFunc;