const writeNewFunc = (firebase, state, setorKey) => {
    var content = {
        login: state.login,
        senha: state.senha,
        cpf: state.cpf,
        nome: state.nome,
        telefone: state.telefone,
        endereco: state.endereco
    }
    var newKey = firebase.database().ref('Setores/' + setorKey + '/Funcionarios').push().key
    var rootRef = firebase.database().ref('Setores/' + setorKey + '/Funcionarios').child(newKey)
    rootRef.set(content)
} 
export default writeNewFunc;