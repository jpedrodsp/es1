const writeNewSolicitation = (firebase, resourceKey, solicitanteKey, state) => {
    var content = {
        resouceKey: resourceKey,
        solicitanteKey: solicitanteKey,
        motivoSolicitacao: state.motivo,
        inicioSolicitacao: state.inicio,
        fimSolicitacao: state.fim,
        solicitacaoAprovada: false
    }
    var newKey = firebase.database().ref('Solicitacoes').push().key
    var rootRef = firebase.database().ref('Solicitacoes').child(newKey)
    rootRef.set(content)
} 
export default writeNewSolicitation;