//readAllSolicitation
const readAllSolicitation = (firebase) => {
    var solicitationRef = firebase.database().ref('Solicitacoes');
    var Solicitacoes = []
    solicitationRef.on('value', function(solicitations) {
        solicitations.forEach(function(childSolicitation) {
            let solicitation = {
                solicitationKey: childSolicitation.key,
                resouceKey: childSolicitation.resourceKey,
                solicitanteKey: childSolicitation.solicitanteKey,
                motivoSolicitacao: childSolicitation.motivoSolicitacao,
                inicioSolicitacao: childSolicitation.inicioSolicitacao,
                fimSolicitacao: childSolicitation.fimSolicitacao,
                solicitacaoAprovada: childSolicitation.solicitacaoAprovada
            }
            Solicitacoes = [...Solicitacoes, solicitation]
        });
    });
    return(
        {Sectors}
    )
}

export default readAllSolicitation;