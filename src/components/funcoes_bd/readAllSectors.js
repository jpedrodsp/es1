const readAllSectors = (firebase) => {
    var setorRef = firebase.database().ref('Setores');
    var Sectors = []
    setorRef.on('value', function(setores) {
        setores.forEach(function(childSetores) {
            let setor = {
                key: childSetores.key,
                nome: childSetores.val().nome
            }
            Sectors = [...Sectors, setor]
        });
    });
    return(
        {Sectors}
    )
}

export default readAllSectors;