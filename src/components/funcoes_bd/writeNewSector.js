const writeNewSector = (valor, firebase) => {
    var content = {
        nome: valor
    }
    var newKey = firebase.database().ref('Setores').push().key
    var rootRef = firebase.database().ref('Setores').child(newKey)
    rootRef.set(content)
} 
export default writeNewSector;