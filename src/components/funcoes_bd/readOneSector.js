const readOneSector = (firebase, nomeSetor) => {
    var setorRef = firebase.database().ref('Setores');
    var Sectors = []
    setorRef.on('value', function(setores) {
        setores.forEach(function(childSetores) {
            if(nomeSetor === childSetores.val().nome){
                let setor = {
                key: childSetores.key,
                nome: childSetores.val().nome
                }
                Sectors = [...Sectors, setor]
            }
        });
    });
    return(
        {Sectors}
    )
}

export default readOneSector;