import React, {Component} from 'react'
import {PrimaryButton, Label, TextField} from 'office-ui-fabric-react'

class InformacaoSolicitacao extends Component {
    state = {
        recurso: {
            name: undefined,
            description: undefined,
            state: undefined,
            solicitante: undefined,
            devolvido: undefined,
            data_aprovacao: undefined,
            data_entrega: undefined,
            data_devolucao: undefined
        }
    }
    alocarRecurso = () => {

    }
    registrarEntrega = () => {
        
    }
    registrarDevolucao = () => {
        
    }
    render() {
        return(
            <div className="informacao-solicitacao-container">
                <TextField label={"Recurso"}/>
                <TextField label={"Descrição"}/>
                <TextField label={"Estado"}/>
                <TextField label={"Solicitante"} disabled={true}/>
                <TextField label={"Devolvido"} disabled={true}/>
                <div className="solicitacao-datas">
                    <TextField label={"Data de Aprovação"} />
                    <TextField label={"Data de Entrega"} />
                    <TextField label={"Data de Devolução"} />
                </div>
                <div className="container-buttons">
                    <PrimaryButton>Alocar recurso para solicitante</PrimaryButton>
                    <PrimaryButton>Registrar entrega</PrimaryButton>
                    <PrimaryButton>Registrar devolução</PrimaryButton>
                </div>
            </div>
        )
    }
}

export default InformacaoSolicitacao